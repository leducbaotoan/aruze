<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aruze');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!%nAba5+WBEF#OBfG|S:F<Q |KksyMOW;Orw@/43h~(xsFo-BhY6i,*.]m&6yR6!');
define('SECURE_AUTH_KEY',  'u|>5n&u2SJwL8-P{=:EoC+@h -rg]W}76lB.VisHQ^pP-YRU$1>}AGRlh+iZB7A$');
define('LOGGED_IN_KEY',    'V9%r8/H|lk+||r:px3F{8LQw_xkPFEh!|$wQw-E0IF).kFOH0GMg,XSb*+Sc`cEE');
define('NONCE_KEY',        '!d0^m^t$WjO@o`*-[j[-g>*).cnv:.G+#O&~-<Fp:DAE6^<c8JgNZId/h5^M:T1|');
define('AUTH_SALT',        '1#uOFBa_+3rOg[acR$.:|C-p+?o>eow_p9K)+66Tf=/W^sl[zF[$=nzyfu@U2jS8');
define('SECURE_AUTH_SALT', 'a 5[$!4L^peaOzWcmEe4`&_NZ~Ti-3OWJ&4/r-6Rf{SQo9hAAyw$Q8-TEhSF^x{?');
define('LOGGED_IN_SALT',   'IhSD[lRuU87Y4 =VQO(N&kZ<],@A<m`2HhJ7@~sJ? z,t4XZ@2-T@Gr]36[:-R^!');
define('NONCE_SALT',       ')DR+Cu;ZR2B,taGu?O:(XU:]M@;wp3r-~Cp=Zrl%vl;i(w9-fZtb}kY#ql<U1*)<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'aruze';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
