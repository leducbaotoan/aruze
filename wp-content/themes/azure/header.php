<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Aruze</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
        <?php global $aruze_opt;?>
        <style type="text/css" media="screen">
            a{color:<?php echo $aruze_opt['color']; ?>;}
            #menu .nav-menu > li > ul > li:hover{background: <?php echo $aruze_opt['color']; ?>;}
            #menu .nav-menu > li > a:hover, #menu .nav-menu > li > a.active{color:<?php echo $aruze_opt['color']; ?>;}
            article #main-content .item p.readmore a.btn{border: 1px solid <?php echo $aruze_opt['color']; ?>;}
            article #main-content .item p.readmore a.btn:hover{background: <?php echo $aruze_opt['color']; ?>;}
            body {
                font-family: <?php echo $aruze_opt['typography']['font-family']; ?>;
                font-weight: <?php echo $aruze_opt['typography']['font-style']; ?>;
                color: <?php echo $aruze_opt['typography']['color']; ?>;
                line-height: <?php echo $aruze_opt['typography']['line-height']; ?>;
            }
        </style>
    </head>
    <body <?php body_class(); ?>>
        <section id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <ul class="social-icons">
                            <li><a target="_blank" href="<?php echo $aruze_opt['facebook'] != '' ? $aruze_opt['facebook'] : '#'; ?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['twitter'] != '' ? $aruze_opt['twitter'] : '#'; ?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['googleplus'] != '' ? $aruze_opt['googleplus'] : '#'; ?>"><i class="fa fa-google-plus"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['pinterest'] != '' ? $aruze_opt['pinterest'] : '#'; ?>"><i class="fa fa-pinterest"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['youtube'] != '' ? $aruze_opt['youtube'] : '#'; ?>"><i class="fa fa-youtube"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['linkedin'] != '' ? $aruze_opt['linkedin'] : '#'; ?>"><i class="fa fa-linkedin"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['dribbble'] != '' ? $aruze_opt['dribbble'] : '#'; ?>"><i class="fa fa-dribbble"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['behance'] != '' ? $aruze_opt['behance'] : '#'; ?>"><i class="fa fa-behance"></i></a></li>
                            <li><a target="_blank" href="<?php echo $aruze_opt['flickr'] != '' ? $aruze_opt['flickr'] : '#'; ?>"><i class="fa fa-flickr"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <ul class="contact-info">
                            <li class="contact-phone"><i class="fa fa-phone"></i> <?php echo $aruze_opt['phone']; ?></li>
                            <li class="contact-email"><i class="fa fa-envelope"></i> <a href="mailto: <?php echo $aruze_opt['email']; ?>"> <?php echo $aruze_opt['email']; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HEADER -->
        <section id="logo">
            <div class="container">
                <div class="logo text-center">
                    <a href="<?php echo home_url(); ?>">
                        <?php if($aruze_opt['logo_main_text'] != '' || $aruze_opt['logo_sub_text'] != ''): ?>
                            <h1><?php echo $aruze_opt['logo_main_text'] != '' ? $aruze_opt['logo_main_text'] : '';; ?></h1>
                            <p class="slogan"><?php echo $aruze_opt['logo_sub_text'] != '' ? $aruze_opt['logo_sub_text'] : '';; ?></p>
                        <?php else: ?>
                            <img src="<?php echo $aruze_opt['logo']; ?>">
                        <?php endif; ?>
                    </a>
                </div>
            </div>
        </section>
        <!-- END LOGO -->
        <section id="menu">
            <div class="container">
                <a id="offcanvas-toggler" class="visible-xs" href="#"><i class="fa fa-bars"></i></a>
                <div id="logo-small" class="col-xs-6 col-sm-2 col-md-2">
                    <h1><a href="<?php echo home_url(); ?>">Aruze</a></h1>
                </div>
                <?php get_nav_menu('main_menu','nav-menu hidden-xs col-md-10 col-sm-10'); ?>
            </div>
        </section>
        <!-- END MENU -->
        <section class="canvas-wrapper"></section>
        <section id="offcanvas-menu">
            <a href="#" class="close-offcanvas"><i class="fa fa-remove"></i></a>
            <?php get_nav_menu('main_menu'); ?>
        </section>
        <!-- END CANVAS -->