<?php 
    /*
     * Template Name: Right Column
     */
?>
<?php get_header(); ?>
<section id="slider">
    <div class="container">
        <ul class="bxslider">
            <?php
                global $aruze_opt;
                $slides = $aruze_opt['slides'];
                foreach ($slides as $slide): ?>
                    <li>
                        <img src="<?php echo $slide['image'] ?>">
                        <div class="slide-caption">
                            <h3><?php echo $slide['title']; ?></h3>
                            <h2><?php echo $slide['description']; ?></h2>
                            <a href="<?php echo $slide['url']; ?>">Read more</a>
                        </div>
                    </li>
            <?php endforeach ?>
        </ul>
    </div>
</section>
<!-- END SLIDER -->
<article>
    <div class="container">
        <div class="row">
            <section id="main-content" class="col-sm-9 col-md-9">
            <?php 
                $args = array(
                    'posts_per_page'=>$aruze_opt['number'] + 1,
                    'paged' => get_query_var('paged'),
                );
                query_posts($args);
                $leading = true;
                $main_content = '';
                if( have_posts() ) :
                    while ( have_posts() ) :
                        the_post();
                        if($leading){
                            ob_start();
                            get_template_part('includes/item');
                            $lead_content = ob_get_contents();
                            ob_clean();
                            $leading = false;
                        }else{
                            ob_start();
                            ?> <div class="col-sm-6"> <?php
                            get_template_part('includes/item');
                            ?> </div> <?php
                            $content = ob_get_contents();
                            ob_clean();
                            $main_content .= $content;
                        }
                endwhile;wp_reset_query();endif; 
            ?>
                    <div class="leading">
                        <?php echo $lead_content ; ?>
                    </div>
                <!-- END ITEM LEADING -->
                <div class="row normal-post">
                    <?php echo $main_content ; ?>
                </div>
                <!-- END NORMAL POST -->
                <?php
                    $my_query = new WP_Query($args);
                    wp_pagenavi(array('query' => $my_query)); 
                ?>      
                <!-- END PAGINATION -->
            </section>
            <!-- END MAIN CONTENT -->
            <?php get_sidebar(); ?>
        </div>
    </div>
</article>        
<!-- END ARTICLE -->
<?php get_footer(); ?>