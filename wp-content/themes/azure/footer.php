<?php 
    global $aruze_opt;
 ?>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                <?php dynamic_sidebar('footer-sidebar' ); ?>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <span><?php echo $aruze_opt['copyright']; ?></span>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->
        <?php wp_footer(); ?>
    </body>
</html>