<div class="item ">
    <?php echo setup_content(get_post_format()); ?>
    <div class="entry-header">
        <span class="post-format"><i class="fa <?php echo get_icon_post_format(get_the_id()); ?>"></i></span>
        <h2>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
    </div>
    <div class="entry-content">
        <?php the_excerpt(); ?>
    </div>
    <dl class="article-info">
        <dt class="article-info-term"></dt> 
        <dd class="createdby">
            <i class="fa fa-user"></i>
            <span itemprop="name" data-toggle="tooltip" title="Written by <?php the_author(); ?>"><?php the_author(); ?></span>
        </dd>           
        <?php
        $categories = get_the_category();
        $string = ''; 
        foreach ( $categories as $category ) { 
            $string .= $category->name; 
        }?>
        <dd class="category-name">
            <i class="fa fa-folder-open-o"></i>
            <a href="/aruze/index.php" itemprop="genre" data-toggle="tooltip" title="Article Category" ><?php echo $string; ?></a>  
        </dd>           
            
        <dd class="published">
            <i class="fa fa-calendar-o"></i>
            <time data-toggle="tooltip" title="Published Date"><?php the_date('d F Y') ?></time>
        </dd>           
        <dd class="hits">
            <span class="fa fa-eye"></span>
            Hits: <?php echo get_view_count(); ?>
        </dd>                  
    </dl>
    <?php if(!is_single()): ?>
        <p class="readmore"><a class="btn btn-default" href="<?php the_permalink(); ?>" itemprop="url">Keep reading </a></p>
    <?php endif; ?>
</div>