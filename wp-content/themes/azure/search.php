<?php
/*
Template Name: Search Page
*/
?>
<?php
    get_header();
?>
<article>
    <div class="container">
        <div class="row">
            <section id="main-content" class="col-sm-9 col-md-9">
				<div class="search">
					<h1 class="page-title">
						Search	
					</h1>

					<form id="searchForm" action="<?php echo home_url( '/' ); ?>" method="get" role="search">

						<div class="btn-toolbar">
							<div class="btn-group pull-left">
								<input type="text" name="s" placeholder="Search Keyword:" id="search-searchword" size="30" maxlength="200" value="<?php echo get_search_query() ?>" class="form-control">
							</div>
							<div class="btn-group pull-left">
								<button onclick="this.form.submit()" class="btn hasTooltip" title="" data-original-title="Search"><span class="fa fa-search"></span></button>
							</div>
						</div>

					</form>
				</div>
				<?php
                    global $aruze_opt;
                    $args = array(
                        'posts_per_page'=>$aruze_opt['number'] + 1,
                        'paged' => get_query_var('paged'),
                        's' => get_search_query(),
                        'post_type' => 'post'
                    );
                    query_posts($args);
                    if( have_posts() ) :
                        while ( have_posts() ) :
                            the_post();
                                ob_start();
                ?>
                                <li class="clearfix">
                                    <h3>
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?></a>
                                    </h3>
                                </li>
                <?php

                                $content = ob_get_contents();
                                ob_clean();
                    endwhile;wp_reset_query();endif; 
                ?>
                <div class="row normal-post">
                    <ul class="category list-striped">
                        <?php echo $content ; ?>
                    </ul>
                </div>
                <!-- END NORMAL POST -->
                <?php
                    $my_query = new WP_Query($args);
                    wp_pagenavi(array('query' => $my_query)); 
                ?>      
                <!-- END PAGINATION -->
            </section>
            <!-- END MAIN CONTENT -->
            <?php get_sidebar(); ?>
        </div>
    </div>
</article>        
<!-- END ARTICLE -->
<?php get_footer(); ?>