<?php
/**
 * Adds Info Widget widget.
 */
class Info_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'info_widget', // Base ID
			__( 'Info', 'text_domain' ), // Name
			array( 'description' => __( 'A Info Widget For Sub Footer', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	?>
    <div class="col-sm-6 col-md-3">
	    <div class="module">
	        <h3 class="module-title">
	            <?php echo $instance['title']; ?>
	        </h3>
	        <div class="module-content">
	            <div class="customcontact">
	                <ul>
                        <li><p><span class="fa fa-location-arrow"></span><?php echo $instance['address']; ?></p>
                        </li>
                        <li><p><span class="fa fa-phone-square"></span><?php echo $instance['phone']; ?></p>
                        </li>
                        <li><p><span class="fa fa-envelope-o"></span><a href="mailto:<?php echo $instance['mail']; ?>"><?php echo $instance['mail']; ?></a></p>
                        </li>
                        <li><p><span class="fa fa-home"></span><a href="<?php echo $instance['website']; ?>"><?php echo $instance['website']; ?></a></p>
                        </li>
                    </ul>
	            </div>
	        </div>
	    </div>
    </div>
	<?php
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'INFO POST', 'text_domain' );
		$address = ! empty( $instance['address'] ) ? $instance['address'] : '';
		$phone = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
		$mail = ! empty( $instance['mail'] ) ? $instance['mail'] : '';
		$website = ! empty( $instance['website'] ) ? $instance['website'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Phone:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mail' ); ?>"><?php _e( 'Mail:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'mail' ); ?>" name="<?php echo $this->get_field_name( 'mail' ); ?>" type="text" value="<?php echo esc_attr( $mail ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'website' ); ?>"><?php _e( 'Website:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'website' ); ?>" name="<?php echo $this->get_field_name( 'website' ); ?>" type="text" value="<?php echo esc_attr( $website ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? $new_instance['address'] : '';
		$instance['phone'] = ( ! empty( $new_instance['phone'] ) ) ? $new_instance['phone'] : '';
		$instance['mail'] = ( ! empty( $new_instance['mail'] ) ) ? $new_instance['mail'] : '';
		$instance['website'] = ( ! empty( $new_instance['website'] ) ) ? $new_instance['website'] : '';

		return $instance;
	}

} // class Info_Widget
