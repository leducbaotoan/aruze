<?php
/**
 * Adds Banner Widget widget.
 */
class Tags_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'tags_widget', // Base ID
			__( 'Tags', 'text_domain' ), // Name
			array( 'description' => __( 'A Tags Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$tags = get_tags();
	?>
	    <div class="module">
	        <h3 class="module-title">
	            <?php echo $instance['title']; ?>
	        </h3>
	        <div class="module-content">
		        <div class="popular-tags">
				<?php if($tags): ?>
		                <ul>
						<?php foreach ( $tags as $tag ) {?>
							<?php $tag_link = get_tag_link( $tag->term_id ); ?>
		                    <li>
		                        <a href="<?php echo $tag_link; ?>">
		                        	<?php echo $tag->name; ?>
		                     		<span class="tag-count badge"><?php echo $tag->count != 0 ? $tag->count : 0; ?></span>
		                        </a>
		                    </li>
	            		<?php } ?>
		                </ul>
	            <?php else: ?>
	            	<p>Do not have tags.</p>
				<?php endif; ?>
	            </div>
	        </div>
	    </div>
	<?php
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'POPULAR TAGS', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

} // class Tags_Widget
