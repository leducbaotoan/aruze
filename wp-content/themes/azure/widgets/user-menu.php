<?php
/**
 * Adds Banner Widget widget.
 */
class User_Menu_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'user_menu_widget', // Base ID
			__( 'User Menu', 'text_domain' ), // Name
			array( 'description' => __( 'A User Menu Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	?>
	    <div class="module">
	        <h3 class="module-title">
	            <?php echo $instance['title']; ?>
	        </h3>
	        <div class="module-content">
                <ul class="side-menu">
            	<?php if(is_user_logged_in()): ?>
	                <li><a href="<?php echo admin_url(); ?>">Site Administrator</a></li>
	                <li><a href="<?php echo wp_logout_url(home_url()); ?>">Log out</a></li>
	            <?php else: ?>
	                <li><a href="<?php echo wp_login_url(home_url()); ?>">Log in</a></li>
	                <li><a href="<?php echo wp_lostpassword_url(home_url()) ?>">Lost Password</a></li>
	            <?php endif; ?>
                </ul>
	        </div>
	    </div>
	<?php
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'USER MENU', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

} // class User_Menu_Widget
