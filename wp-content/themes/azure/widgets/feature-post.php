<?php
/**
 * Adds Banner Widget widget.
 */
class Feature_Post_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Feature_post_widget', // Base ID
			__( 'Feature Post', 'text_domain' ), // Name
			array( 'description' => __( 'A Feature Post Widget For Sub Footer', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$posts = get_posts( 'posts_per_page=10' );
	?>
    <div class="col-sm-6 col-md-3">
	    <div class="module">
	        <h3 class="module-title">
	            <?php echo $instance['title']; ?>
	        </h3>
	        <div class="module-content">
				<ul class="bxslider-2">
				<?php if($posts): ?>
					<?php foreach ( $posts as $post ) { ?>
		                <li>
                            <a href="<?php echo get_permalink($post->ID); ?>">
                            	<?php echo get_the_post_thumbnail($post->ID); ?>
                            </a>
                            <h3 class="title"><?php echo $post->post_title; ?></h3>
                            <p><?php echo $post->post_excerpt; ?></p>
                        </li>
            		<?php } ?>
	            <?php else: ?>
	            	<p>Do not have posts.</p>
				<?php endif; ?>
	            </ul>
	        </div>
	    </div>
    </div>
	<?php
		wp_reset_query();
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'FEATURE POST', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

} // class Feature_Post_Widget
