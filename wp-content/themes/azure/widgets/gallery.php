<?php
/**
 * Adds Banner Widget widget.
 */
class Gallery_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'gallery_widget', // Base ID
			__( 'Gallery', 'text_domain' ), // Name
			array( 'description' => __( 'A Gallery Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $aruze_opt;
		$images = explode(',',$aruze_opt['gallery']);
	?>
	    <div class="module">
	        <h3 class="module-title">
	            <?php echo $instance['title']; ?>
	        </h3>
	        <div class="module-content">
	            <div class="gallery">
				<?php if( $images ): ?>
	                <ul>
        			<?php foreach( $images as $image ): ?>
        				<?php $image = wp_get_attachment_url($image); ?>
	                    <li>
	                        <a href="<?php echo $image; ?>" rel="lightbox-atomium">
	                     		<img src="<?php echo $image; ?>" />
	                        </a>
	                    </li>
        			<?php endforeach; ?>
	                </ul>
	            <?php else: ?>
	            	<p>Do not have images.</p>
				<?php endif; ?>
	            </div>
	        </div>
	    </div>
	<?php
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'GALLERY LIGHTBOX', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

} // class Gallery_Widget
