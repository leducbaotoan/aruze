<?php
    get_header();
?>
<article>
    <div class="container">
        <div class="row">
            <section id="main-content" class="col-sm-9 col-md-9">
                <div class="normal-post">
                    <?php 
                        if( have_posts() ) :
                            while ( have_posts() ) :
                                the_post();
                                view_count();
                                get_template_part('includes/item');
                            endwhile;
                            wp_reset_query();
                        endif; 
                    ?>
                </div>
                <?php echo get_pager(true); ?>
            </section>
            <!-- END MAIN CONTENT -->
            <?php get_sidebar(); ?>
        </div>
    </div>
</article>        
<!-- END ARTICLE -->
<?php get_footer(); ?>