jQuery(document).ready(function($) {
	$('.bxslider').bxSlider({
	  infiniteLoop: true,
	  hideControlOnEnd: true,
	  auto: true,
	});
	$('.bxslider-2').bxSlider({
	  infiniteLoop: true,
	  hideControlOnEnd: true,
	  auto: true,
	  pager: false,
	});
	$('.gallery-slider').bxSlider({
	  infiniteLoop: true,
	  hideControlOnEnd: true,
	  auto: true,
	  pager: false,
	});
});