<?php

	define('URI', get_template_directory_uri());

    add_filter('show_admin_bar', '__return_false');
    if ( function_exists( 'add_theme_support' ) ) { 
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-formats', array( 'standard', 'video', 'gallery', 'audio', 'quote', 'status' ) );
    }

	require_once (dirname(__FILE__).'/includes/admin-config.php');
	require_once (dirname(__FILE__).'/widgets/banner.php');
	require_once (dirname(__FILE__).'/widgets/gallery.php');
	require_once (dirname(__FILE__).'/widgets/last-post.php');
	require_once (dirname(__FILE__).'/widgets/online.php');
	require_once (dirname(__FILE__).'/widgets/tags.php');
	require_once (dirname(__FILE__).'/widgets/user-menu.php');
	require_once (dirname(__FILE__).'/widgets/feature-post.php');
	require_once (dirname(__FILE__).'/widgets/menu-footer.php');
	require_once (dirname(__FILE__).'/widgets/info.php');
	require_once (dirname(__FILE__).'/widgets/aboutme.php');

	function admin_init(){

		register_nav_menus( array(
			'main_menu' => 'My Custom Main Menu',
			'footer_menu' => 'My Custom Footer Menu',
		) );
	}
	add_action( 'admin_init', 'admin_init' );

	function regis_widgets(){
		register_sidebar( array(
	        'name' => __( 'Main Sidebar', 'theme-slug' ),
	        'id' => 'main-sidebar',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	    ) );

		register_sidebar( array(
	        'name' => __( 'Sub Sidebar', 'theme-slug' ),
	        'id' => 'sub-sidebar',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	    ) );

	    register_sidebar( array(
	        'name' => __( 'Footer Sidebar', 'theme-slug' ),
	        'id' => 'footer-sidebar',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	    ) );

	    register_widget( 'Banner_Widget' );
	    register_widget( 'Gallery_Widget' );
	    register_widget( 'Last_Post_Widget' );
	    register_widget( 'Online_Widget' );
	    register_widget( 'Tags_Widget' );
	    register_widget( 'Feature_Post_Widget' );
	    register_widget( 'User_Menu_Widget' );
	    register_widget( 'Menu_Footer_Post_Widget' );
	    register_widget( 'About_Widget' );
	    register_widget( 'Info_Widget' );

    	unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Text');
		unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
		unregister_widget('WP_Nav_Menu_Widget');
		unregister_widget('Twenty_Eleven_Ephemera_Widget');
	}
	add_action( 'widgets_init', 'regis_widgets' );

    /**
	 * Enqueue scripts & styles
	 *
	 */
	function aruze_scripts() {
		wp_enqueue_style( 'main', URI.'/css/main.css', array('bxslider'));
		wp_enqueue_style( 'bxslider', URI.'/css/jquery.bxslider.css');
		wp_enqueue_style( 'slimbox', URI.'/css/slimbox.css');

		wp_enqueue_script('mainjs',URI.'/js/main.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('bootstrap',URI.'/js/bootstrap.min.js', array('jquery'), 'v3.3.6', true);
		wp_enqueue_script('bxslider',URI.'/js/jquery.bxslider.min.js', array('jquery'), 'v4.1.2', true);
		wp_enqueue_script('easing',URI.'/js/jquery.easing.1.3.js', array('jquery'), '1.3.0', true);
		wp_enqueue_script('fitvids',URI.'/js/jquery.fitvids.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('mootools',URI.'/js/mootools.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('slimbox',URI.'/js/slimbox.js', array('jquery'), '1.0.0', true);

		// Register the script
		wp_register_script( 'bxslider-options', URI.'/js/bxslider.js' );
		// Localize the script with new data
		$slide_options = array(
			'some_string' => __( 'Some string to translate', 'plugin-domain' ),
			'a_value' => '10'
		);
		wp_localize_script( 'bxslider-options', 'object_name', $slide_options );

		// Enqueued script with localized data.
		wp_enqueue_script( 'bxslider-options' );
	}

	add_action( 'wp_enqueue_scripts', 'aruze_scripts' );

	function get_nav_menu($locate = false, $classes = false){
		$defaults = array(
			'theme_location'  => $locate,
			'menu'		      => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="'.$classes.'">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
		);

		wp_nav_menu( $defaults );
	}

	function add_custom_field_automatically($post_ID) {
		global $wpdb;
		if(!wp_is_post_revision($post_ID)) {
		    add_post_meta($post_ID, 'view', 0, true);
		}
	}
	add_action('publish_page', 'add_custom_field_automatically');
	add_action('publish_post', 'add_custom_field_automatically');

	function get_view_count($id = false){
		if(!$id){
			$id = get_the_id();
		}
		return get_post_meta($id,'view', true);
	}

	function view_count($id = false){
		if(!$id){
			$id = get_the_id();
		}
		$view = get_view_count($id);
		$view++;
		update_post_meta($id, 'view', $view);
		return $view++;
	}

	function get_icon_post_format($id = false){
		if(!$id){
			$post_format = 'standard';
		}else{
			$post_format = get_post_format($id) ? get_post_format($id) : 'standard';
		}
		$args = array(
			'standard' => 'fa-thumb-tack',
			'video' => 'fa-video-camera',
			'gallery' => 'fa-picture-o',
			'audio' => 'fa-music',
			'quote' => 'fa-quote-left',
			'status' => 'fa-comment-o'
			);
		return $args[$post_format];
	}

	function setup_content($format){
		switch ($format) {
			case 'video':
				return '<div class="entry-video">'.get_field($format, get_the_id()).'</div>';
				break;
			case 'audio':
				return '<div class="entry-audio">'.get_field($format, get_the_id()).'</div>';
				break;
			case 'quote':
				ob_start();
				$quotes = get_field($format, get_the_id());
				foreach ($quotes as $quote) {
				?>
					<div class="entry-quote">
						<blockquote>
							<p><?php echo $quote['content']; ?></p>
							<small><?php echo $quote['author']; ?></small>
						</blockquote>
					</div>
				<?php
				}
				$content = ob_get_contents();
				ob_clean();
				return $content;
				break;
			case 'status':
				return '<div class="entry-status">'.get_field($format, get_the_id()).'</div>';
				break;
			case 'gallery':
				ob_start();
				$quotes = get_field($format, get_the_id());
				?>
				<div class="entry-gallery">
					<ul class="gallery-slider">
				<?php
				foreach ($quotes as $image) {
				?>
					<li><img src="<?php echo $image['url']; ?>"></li>
				<?php
				}
				?>
					</ul>
				</div>
				<?php
				$content = ob_get_contents();
				ob_clean();
				return $content;
				break;
			default:
				if(has_post_thumbnail()){
					return '<div class="entry-image"><a href="'.get_the_permalink().'">'.get_the_post_thumbnail().'</a></div>';
				}else{
					return null;
				}
				break;
		}
	}

	function get_pager($in_same_terms = false, $taxonomy = 'category'){
		$next_post = get_next_post($in_same_terms, '', $taxonomy);
		$previous_post = get_previous_post($in_same_terms, '', $taxonomy);
    	ob_start();
    	if($next_post != '' || $previous_post != ''){
	?>
			<ul class="pager pagenav">
				<?php if($previous_post != ''): ?>
		            <li class="previous">
		                <a href="<?php echo get_permalink( $previous_post->ID ); ?>" rel="prev"><span class="glyphicon glyphicon-menu-left"></span>Prev</a>
		            </li>
	        	<?php endif; ?>
				<?php if($next_post != ''): ?>
		            <li class="next">
		                <a href="<?php echo get_permalink( $next_post->ID ); ?>" rel="next">Next<span class="glyphicon glyphicon-menu-right"></span></a>
		            </li>
	        	<?php endif; ?>
	        </ul>
    <?php
    	}
    	$html = ob_get_contents();
    	ob_clean();
    	return $html;
	}

	function my_search_form( $form ) {
		$form = '<form role="search" method="get" class="search-form" action="'.home_url( '/' ).'">
			<input type="search" class="form-control" placeholder="Type and hit Enter..." value="'.get_search_query().'" name="s"/>
		</form>';

		return $form;
	}

	add_filter( 'get_search_form', 'my_search_form' );
